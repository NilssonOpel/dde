#!/usr/bin/python3
# coding=UTF-8
''' The startup of DDE'''

import contextlib
import os
from pathlib import Path
import re
import subprocess
import sys

import le_gui as my_gui

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def le_log(text, do_print=False):
    '''Poor mans debug print'''
    if do_print:
        print(text)

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def ccp():
    '''Get current code page'''
    if sys.platform == 'win32':
        try:
            return ccp.codepage
        except AttributeError:
            reply = os.popen('cmd /c CHCP').read()
            cp = re.match(r'^.*:\s+(\d*)$', reply)
            if cp:
                ccp.codepage = cp.group(1)
            else:
                ccp.codepage = 'utf-8'
            return ccp.codepage
    return 'utf-8'

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def get_root_dir(path, ext):
    '''Get root dir with extension ext
    Mainly used to find .git and .svn working directories'''
    if os.path.isdir(path):
        the_dir = path
    else:
        the_dir = os.path.dirname(path)
    curr_dir = os.path.realpath(the_dir)
    while True:
        a_root = os.path.join(curr_dir, ext)
        le_log(f'Looking for {a_root}')
        if os.path.exists(a_root):
            break
        next_dir = os.path.split(curr_dir)[0]
        if next_dir == curr_dir:
            le_log(f'Giving up at {curr_dir}')
            return None
        curr_dir = next_dir

    curr_dir = Path(curr_dir).resolve()
    return curr_dir


#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def is_in_svn(file):
    svn_dir = get_root_dir(file, '.svn')
    if svn_dir is None:
        return False

    # Check if file is known by svn
    commando = f'svn info {file}'
    reply = run_process(commando, False, use_shell=True)
    if 'was not found' in reply:
        le_log(f'svn info returned: {reply}')
        return False

    return True

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def is_in_git(file):
    git_dir = get_root_dir(file, '.git')
    if git_dir is None:
        le_log('No .git found')
        return False

    # Make a pushd to the git dir
    with chdir(git_dir):

        # Check if directory is known by git
        command = 'git log -1'
        reply = run_process(command, False, use_shell=True)
        if len(reply) <= 2:
            le_log(f'git log returned: {reply}')
            return False

    return True

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def filtered_path(path):
    '''Our default filtering'''
    if path.endswith('.svn'):
        return True
    if path.endswith('.git'):
        return True
    return False

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def run_process(command, do_check, use_shell=False, extra_dir=os.getcwd()):
    try:
        status = subprocess.run(command,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                text=True,
                                shell=use_shell,
                                encoding=ccp(),  # See https://bugs.python.org/issue27179
                                check=do_check)
        if status.returncode == 0:
            reply = status.stdout
        else:
            reply = status.stdout
            reply += status.stderr

    except Exception as e:
        if extra_dir:
            command = f'At {extra_dir}:\n'
        reply = f'{command} threw an exception:\n'
        reply += f'type: {type(e)}\n'
        reply += f'    : {e}\n'
        reply += '\n-end of exception-\n'
        reply += f'stdout: {e.stdout}\n'
        reply += f'stderr: {e.stderr}\n'

    return reply

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------

@contextlib.contextmanager
def fallback_chdir(path):
    curr_dir = os.getcwd()
    try:
        os.chdir(path)
        yield
    finally:
        os.chdir(curr_dir)

#----------------------------------------------------------------------

try:
    chdir = contextlib.chdir      # >= Python 3.11
except AttributeError:
    chdir = fallback_chdir

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def main():
    my_gui.the_gui()


#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
if __name__ == "__main__":
    sys.exit(main())
