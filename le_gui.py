#!/usr/bin/python3
# coding=UTF-8

from idlelib.redirector import WidgetRedirector
import os
import subprocess
import sys
import tkinter as tk
import tkinter.messagebox as tkMessageBox
import tkinter.ttk as ttk
import ttkthemes as ttk_themes

import le_configs as my_config
import le_dirview
import le_tortoise
import dde

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def dumpTextEvent(event):
    dde.le_log(80*'-')
    dde.le_log(vars(event))
    widget = event.widget
    widget.update()
    text = widget.get(tk.SEL_FIRST, tk.SEL_FIRST+"lineend")
    dde.le_log(f'Line: {text}')
    dde.le_log(80*'-')
    dde.le_log('\n')

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
# https://stackoverflow.com/questions/3842155/is-there-a-way-to-make-the-tkinter-text-widget-read-only

class ReadOnlyText(tk.Text):
    def __init__(self, *args, **kwargs):
        tk.Text.__init__(self, *args, **kwargs)
        self.redirector = WidgetRedirector(self)
        self.insert = self.redirector.register("insert", lambda *args, **kw: "break")
        self.delete = self.redirector.register("delete", lambda *args, **kw: "break")


class TextTab:
    def __init__(self, parent, options, *args, **kwargs):
        self.parent = parent
        self.options = options
        self.bg = 'red'
        if 'bg' in kwargs:
            self.bg = kwargs['bg']
        self.text_frame = tk.Frame(self.parent)
        self.the_text = ReadOnlyText(self.text_frame, selectbackground="khaki1",
            selectforeground="black", bg=self.bg)
        self.the_text.bind('<KeyPress>', self._keyboard_click)
        self.the_text.bind('<Double-Button-1>', self._texttab_select)
        self.the_text.bind('<Button-3>', self._context_menu)

    def _handle_texttab_view(self, event):
        the_key = event.keysym.lower()
        if the_key == 'b':
            self._handle_tortoise(event, 'blame')
        if the_key == 'e':
            self._open_editor(event)
        if the_key == 'f':
            # Could be search in results
            pass
        if the_key == 'l':
            self._handle_tortoise(event, 'log')

    def _handle_tortoise(self, event, tortoise_cmd):
        the_path, line = self._selected_text2file_line(event)
        if not the_path:
            return
        le_tortoise.handle_tortoise(the_path, tortoise_cmd, line)

    def _keyboard_click(self, event):
        dde.le_log("TextTab _keyboard_click")
#        dumpEvent(event)
        self._handle_texttab_view(event)

    def _texttab_select(self, event):
        dde.le_log("TextTab _texttab_select")
#        dumpEvent(event)
        self._open_editor(event)

    def _set_tab_index(self, tab_index):
        self.tab_index = tab_index

    def get_tab_index(self):
        return self.tab_index

    def add_to_tab(self, tabs, header, index):
        tabs.add(self.text_frame, text=header)
        self._set_tab_index(index)

    def get_text_widget(self):
        return self.the_text

    def pack(self):
        self.the_text.pack(expand=1, fill='both')

    def _selected_text2file_line(self, event):
#        dumpEvent(event)
        widget = event.widget
        text = widget.get('insert linestart', 'insert lineend')
        if not text:
            tkMessageBox.showinfo('TextTab', 'No text to select from\n')
            return "", ""
        dde.le_log(f'Text: {text}')
        point = text.split(':')
        dde.le_log(f'Point: {point}')
        # Handle drivletter colon, e.g. D:
        if text[1] == ':':
            point[0] = point[0] + ":" + point[1]
            point[1] = point[2]
            del point[2]
        if len(point) >= 1:
            dde.le_log(f'File: {point[0]}')
        if len(point) >= 2:
            dde.le_log(f'Line: {point[1]}')
        if len(point) >= 3:
            dde.le_log(f'Rest: {point[2]}')
        file = point[0]
        line = point[1]
        return file, line

    def _open_editor(self, event):
        path, line = self._selected_text2file_line(event)
        if not path:
            dde.le_log(f'No path: giving up')
            return
        command = my_config.get_editor()
        if 'line' in command:
            command = command.replace('line', line)
        if 'path' in command:
            command = command.replace('path', path)
        dde.le_log(f'Repl: {command}')
        subprocess.Popen(command, shell=True)

    def _context_menu(self, event):
        ctxMenu = tk.Menu(self.the_text, tearoff=0)
        ctxMenu.add_command(label='Blame  (B)',
            command=self._curry_handle_context_menu(event, 'b', ctxMenu))
        ctxMenu.add_command(label='Edit  (E)',
            command=self._curry_handle_context_menu(event, 'e', ctxMenu))
        ctxMenu.add_command(label='Log  (L)',
            command=self._curry_handle_context_menu(event, 'l', ctxMenu))
        ctxMenu.tk_popup(event.x_root, event.y_root)
        dde.le_log("treeContextMenu")

    def _curry_handle_context_menu(self, event, key, ctxMenu):
        dde.le_log('curry_handleContextEdit')
        widget = event.widget
        widget.update()
#        iid = widget.identify_row(event.y)
#        item = widget.item(iid)

        def handleContextMenu(the_key=key):
            if the_key == 'b':
                self._handle_tortoise(event, 'blame')
            if the_key == 'e':
                self._open_editor(event)
            if the_key == 'l':
                self._handle_tortoise(event, 'log')
            # Since we are creating the ctxMenu dynamically we
            # need to destroy it manually
            ctxMenu.destroy()

        return handleContextMenu

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
class HeadMenu:
    def __init__(self, parent, *args, **kwargs):
        self.top = parent.winfo_toplevel()
        self.menuBar = tk.Menu(self.top)
        self.top['menu'] = self.menuBar

        self.fileMenu = tk.Menu(self.menuBar, tearoff=0)
        self.menuBar.add_cascade(label='File', menu=self.fileMenu)
        self.fileMenu.add_command(label='Exit', command=self._exit_handler)

        self.toolMenu = tk.Menu(self.menuBar, tearoff=0)
        self.menuBar.add_cascade(label='Tool', menu=self.toolMenu)
        self.toolMenu.add_command(label='Options', command=self._handle_options)

        self.helpMenu = tk.Menu(self.menuBar, tearoff=0)
        self.menuBar.add_cascade(label='Help', menu=self.helpMenu)
        self.helpMenu.add_command(label='Cannot quit vi?', command=self._quit_vi)
        self.helpMenu.add_command(label='About', command=self._aboutHandler)


    def _exit_handler(self):
        dde.le_log('In HeadMenu::_exit_handler')
        self.top.quit()

    def _handle_options(self):
        dde.le_log('In HeadMenu::_handle_options')

    def _quit_vi(self):
        tkMessageBox.showinfo('How to quit vi',
            'Without saving:  <esc>:q!<ret>\n'+
            'With saving:  <esc>:wq<ret>\n')

    def _aboutHandler(self):
        dde.le_log('In _aboutHandler')
        tkMessageBox.showinfo('About DDE',
        'DDE - experience the ultimate development experience ' +
        'you ever experienced\n'+
        'Version 4711\n' +
        'https://js-css-poker.sourceforge.io/poker.html')


#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
class TabbedView:
    def __init__(self, parent, options, *args, **kwargs):
        self.tabs = ttk.Notebook(parent)
        self.parent = parent
        self.bg = 'light grey'
        # Log window
        self.log_frame = tk.Frame(self.parent, width=150)
        self.log_var = tk.StringVar()
        self.output = tk.Listbox(self.log_frame, bg=self.bg, listvariable=self.log_var)
        self.tabs.add(self.log_frame, text='Log Window')

        # Find a File
        self.faf = TextTab(self.parent, options, bg=self.bg)
        self.faf.add_to_tab(self.tabs, 'Find a File', 1)

        # Find in Files
        self.fif = TextTab(self.parent, options, bg=self.bg)
        self.fif.add_to_tab(self.tabs, 'Find in Files', 2)

        # rg in Files
        self.rgif = TextTab(self.parent, options, bg=self.bg)
        self.rgif.add_to_tab(self.tabs, 'rg in Files', 3)

        # Show ttk styles in a tab
        s = ttk.Style()
        g = s.theme_names()

        self.ttkstyles_frame = tk.Frame(self.parent)
        self.ttkstyles_list = tk.StringVar()
        self.ttkstyles = tk.Listbox(self.ttkstyles_frame,
            listvariable=self.ttkstyles_list,
            activestyle='none', bg=self.bg)
        self.ttkstyles.pack(expand=1, fill='both')
        self.tabs.add(self.ttkstyles_frame, text='ttk Styles')

        for n in g:
            print_stringvar(self.ttkstyles_list, n)

        #    style_used = s.theme_use('clam')
        style_used = s.theme_use()
        print_stringvar(self.ttkstyles_list, f'Used: {style_used}')

        # Finalize
        self.output.pack(side='top', fill='both', expand=True)
        self.faf.pack()
        self.fif.pack()
        self.rgif.pack()
        self.tabs.pack(expand=1, fill='both')

def print_stringvar(string_var, text):
    raw = string_var.get()
    raw = raw.strip('\',()')
    temp = raw.split(',')
    next = []
    if len(temp[0]) == 0:
        temp.pop(0)
    for str in temp:
        str = str.strip('\',() ')
        if len(str) == 0:
            next
        next.append(str)
    next.append(text)
    string_var.set(next)

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
class MainApplication:
    def __init__(self, parent, options, *args, **kwargs):
        self.parent = parent
        self.parent.title('DDE')

        # Add a menu
        self.menu = HeadMenu(self.parent)

        self.main_pane = tk.PanedWindow(parent, orient=tk.VERTICAL,
            width=1050, bd=0, bg='green')
        self.main_pane.pack(side="top", fill="both", expand=True)

        # Upper frame for dir tree and actions
        self.upper_frame = tk.Frame(parent, width=150, bg='yellow')

        # Lower frame for output
        self.lower_frame = tk.Frame(parent, width=150, bg='red')

        self.dir_view = le_dirview.DirectoryView(self.upper_frame, options)
        self.upper_frame.pack(side="top", fill="both", expand=True)

        self.low = tk.Entry(self.lower_frame, width=150)
        self.low.pack(side="top", fill="both", expand=True)

        self.tabbed_view = TabbedView(self.lower_frame, options)
        self.lower_frame.pack(side="top", fill="both", expand=True)
        # Output from FindAFile etc
        self.dir_view.set_tabview(self.tabbed_view)

        self.main_pane.add(self.upper_frame)
        self.main_pane.add(self.lower_frame)

        self.dir_view.tree.focus_set()

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def dumpEvent(event):
    dde.le_log(80*'-', True)
    dde.le_log(vars(event), True)
    widget = event.widget
    widget.update()
    iid = widget.focus()
    dde.le_log(f'In focus: {iid}', True)
    if hasattr(widget, 'item'):
        item = widget.item(iid)
        dde.le_log(item, True)
    dde.le_log(80*'-', True)
    dde.le_log('\n', True)

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------

# https://www.reddit.com/r/Python/comments/lps11c/how_to_make_tkinter_look_modern_how_to_use_themes/

def the_gui():
    options = my_config.get_the_options()
    dde.le_log(f'options: {options}')
    options['actual_dir'] = os.getcwd()
    if False:
        root = tk.Tk()
        style = ttk.Style(root)
        style.theme_use('clam')
    else:
        root = tk.Tk()
        style = ttk_themes.ThemedStyle(root)
#        style.theme_use('aquativo')    3
#        style.theme_use('black')       3
#        style.theme_use('blue')        2
#        style.theme_use('clearlooks')  3
        style.theme_use('elegance')  #  4
#        style.theme_use('itft1')       2
#        style.theme_use('keramik')     3
#        style.theme_use('kroc')        4
#        style.theme_use('plastik')     3
#        style.theme_use('radiance')   # 3
#        style.theme_use('smog')        2
#        style.theme_use('winxpblue')   3.5
    the_app = MainApplication(root, options)
    root.mainloop()
    dde.le_log(f'options: {options}')
    del options['actual_dir']
#    my_config.save_the_options(options)
