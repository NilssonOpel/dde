#!/usr/bin/python3
# coding=UTF-8

import os
import subprocess
import sys
import tkinter as tk
import tkinter.messagebox as tkMessageBox

import dde


def handle_tortoise_win(the_path, tortoise_cmd, line=None):
    if tortoise_cmd == 'blame' and os.path.isdir(the_path):
        tkMessageBox.showinfo('Blame', 'Cannot blame on a directory\n')
        return

    command = ""
    postlude = f'/command:{tortoise_cmd} /path:{the_path}'
    if line is not None:
        postlude += f' /line:{line}'
    if tortoise_cmd == 'blame':
        postlude += ' /startrev:1 /endrev:-1'

    if dde.is_in_svn(the_path):
        command = 'tortoiseproc'
        postlude += ' /ignoreexternals'     # Makes no difference on speed...
        command = f'tortoiseproc {postlude}'
    elif dde.is_in_git(the_path):
        command = f'tortoisegitproc {postlude}'

    else:
        tkMessageBox.showinfo('Tortoise', f'{the_path} not under version control\n')
        return

    subprocess.Popen(command, shell=True)

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def handle_tortoise_lin(the_path, cmd, line=None):
    if dde.is_in_svn(the_path):
        tkMessageBox.showinfo('Tortoise', f'Sorry, cannot handle subversion yet\n')
        return
    if not dde.is_in_git(the_path):
        tkMessageBox.showinfo('Tortoise', f'{the_path} not under version control\n')
        return

    # So now it is just git---
    if cmd == 'log':
        command = f'gitk {the_path}'
    elif cmd == 'diff':
        command = f'git difftool {the_path}'

    elif cmd == 'blame':
        if os.path.isdir(the_path):
            tkMessageBox.showinfo('Blame', 'Cannot blame on a directory\n')
            return

        command = 'git gui blame'
        if line is not None:
            command += f' --line={line}'
        command += f' {the_path}'

    else:
        if os.path.isdir(the_path):
            the_dir = the_path
        else:
            the_dir = os.path.dirname(the_path)
        with dde.chdir(the_dir):
            command = 'git citool'
            subprocess.Popen(command, shell=True)
            return

    subprocess.Popen(command, shell=True)

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def handle_tortoise(the_path, cmd, line=None):
    if sys.platform == 'win32':
        handle_tortoise_win(the_path, cmd, line)
    else:
        handle_tortoise_lin(the_path, cmd, line)

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def main():
    command = sys.argv[1]
    path = '.'
    if len(sys.argv) > 2:
        path = sys.argv[2]
    line = 1
    if len(sys.argv) > 3:
        line = sys.argv[3]
    abs_path = os.path.abspath(path)
    root = tk.Tk()
    root.overrideredirect(1)
    root.withdraw()
    if sys.platform == 'win32':
        handle_tortoise_win(abs_path, command, line)
    else:
        handle_tortoise_lin(abs_path, command, line)

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
