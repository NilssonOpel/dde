# From https://dafarry.github.io/tkinterbook/tkinter-dialog-windows.htm
# originally from http://effbot.org/tkinterbook/tkinter-dialog-windows.htm

import tkinter as tk
import tkinter.simpledialog as tkSimpleDialog

import dde


class MyDialog(tkSimpleDialog.Dialog):

    def __init(self, parent, title='None'):
        super.init(self, parent, title)

    def body(self, master):
        tk.Label(master, text="Search:").grid(row=0)
        tk.Label(master, text="File pattern:").grid(row=1)
        self.ignore_case = tk.IntVar()
        tk.Radiobutton(master, text="Case insensitive",
            variable=self.ignore_case, value=1).grid(row=3)
        self.e1 = tk.Entry(master)
        self.e2 = tk.Entry(master)

        self.e1.grid(row=0, column=1)
        self.e2.grid(row=1, column=1)
        return self.e1 # initial focus

    def apply(self):            # Will be called on 'Ok'
        first = self.e1.get()
        second = self.e2.get()
        ignore_case = self.ignore_case.get()
        dde.le_log(f'first = {first}, second = {second}, ignore = {ignore_case}') # or something
        if len(first) == 0:
            first = None
        if len(second) == 0:
            second = None
        self.result = first, second, ignore_case


class FindAFileDialog(tkSimpleDialog.Dialog):

    def __init(self, parent, title='None'):
        super.init(self, parent, title)

    def body(self, master):

        tk.Label(master, text="Part of path:").grid(row=0)
        self.e1 = tk.Entry(master)

        self.e1.grid(row=0, column=1)
        return self.e1 # initial focus

    def apply(self):
        first = self.e1.get()
        dde.le_log(f'first = {first}') # or something
        self.result = first
