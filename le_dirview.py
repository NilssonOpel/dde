import fnmatch
import os
import sys
import subprocess

import tkinter as tk
import tkinter.messagebox as tkMessageBox
import tkinter.ttk as ttk

import le_configs as my_config
import le_mbox
import le_ripgrep
import le_tortoise
import dde


def get_dirname(the_path):
    if os.path.isdir(the_path):
        the_dir = the_path
    else:
        the_dir = os.path.dirname(the_path)
    return the_dir

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
class DirectoryView:
    def __init__(self, parent, options, *args, **kwargs):
        self.parent = parent
        self.options = options
        self.dir_frame = tk.Frame(parent, width=150, bg='yellow')

        # Setup the directory view
        self.tree = self._gen_dirview(options['actual_dir'])
        self.tree.bind('<<TreeviewSelect>>', self._treeviewSelect)
        self.tree.bind('<<TreeviewOpen>>'  , self._treeviewOpen)
        self.tree.bind('<<TreeviewClose>>' , self._treeviewClose)
        self.tree.bind('<KeyPress>', self._keyboard_click)
        self.tree.bind('<Button-3>', self._context_menu)

        self.tree.pack(side='top', fill='both', expand=True)
        self.dir_frame.pack(side="left", fill="both", expand=True)

    # https://stackoverflow.com/questions/42708050/tkinter-treeview-heading-styling
    def _prepare_custom_heading(self):
        style = ttk.Style()
        style.element_create("Custom.Treeheading.border", "from", "default")
        style.layout("Custom.Treeview.Heading", [
            ("Custom.Treeheading.cell", {'sticky': 'nswe'}),
            ("Custom.Treeheading.border", {'sticky':'nswe', 'children': [
                ("Custom.Treeheading.padding", {'sticky':'nswe', 'children': [
                    ("Custom.Treeheading.image", {'side':'right', 'sticky':''}),
                    ("Custom.Treeheading.text", {'sticky':'we'})
                ]})
            ]}),
        ])
        style.configure("Custom.Treeview.Heading",
            background="gold2", foreground="black", relief="flat")
        style.map("Custom.Treeview.Heading",
            relief=[('active','groove'),('pressed','sunken')])

    def _gen_dirview(self, path):
        self._prepare_custom_heading()
        tree = ttk.Treeview(self.parent, style="Custom.Treeview")
        heading_text = f'Root dir: {path}'
        tree.heading("#0", text=heading_text, anchor=tk.W)

        abs_path = os.path.abspath(path)
        root_node = tree.insert('', 'end', text=abs_path, tags=repr(abs_path))
        iid = tree.insert(root_node, 'end', text='.', tags=repr(abs_path))
        dde.le_log(f'root_node={root_node}, iid={iid}')
        tree.focus(root_node)
        tree.selection_set(root_node)
        tree.rowconfigure(0, weight=1)
        tree.columnconfigure(0, weight=1)
        tree.grid(row=0, column=0, sticky=tk.N+tk.S+tk.W)
        return tree

    def _add_directory(self, parent, root_path):
        try:
            if dde.filtered_path(root_path):
                oid = self.tree.insert(parent, 'end', text='<skipped>',
                    tags=repr(root_path))
                return
#        dde.le_log(f'Processing {path}')
            for item in os.listdir(root_path):
                item_path = os.path.join(root_path, item)
                oid = self.tree.insert(parent, 'end', text=item,
                    tags=repr(item_path))
                if os.path.isdir(item_path):
                    if len(os.listdir(item_path)) == 0:
                        oid = self.tree.insert(oid, 'end', text='<empty>',
                            tags=repr(item_path))
                    else:
                        oid = self.tree.insert(oid, 'end', text='.',
                            tags=repr(item_path))
        except:
            dde.le_log(f'_add_directory exception at {root_path}')

    def _treeviewSelect(self, event):
        dde.le_log("_treeviewSelect")
#       dumpEvent(event)

    def _treeviewOpen(self, event):
        dde.le_log("_treeviewOpen")
#        dumpEvent(event)
        widget = event.widget
        widget.update()
        oid = widget.focus()
        children = self.tree.get_children(oid)
        dde.le_log(f'Children: {children}')
        item = widget.item(oid)
        the_item = item['tags']
        self.tree.heading("#0", text=the_item, anchor=tk.W)
        if len(children) == 1:
            child_id = children[0]
            child_item = widget.item(child_id)
            if child_item['text'] == '.':
                self.tree.delete(child_id)
                the_item = item['tags']
                the_path = str(the_item[0])
                abs_path = the_path[1:-1]  # UNIFY
                self._add_directory(oid, abs_path)

    def _treeviewClose(self, event):
        dde.le_log("_treeviewClose")
#        dumpEvent(event)
        widget = event.widget
        widget.update()
        oid = widget.focus()
        item = widget.item(oid)
        item_path = item['tags']
        children = self.tree.get_children(oid)
        dde.le_log(f'Children: {children}')
        for child_id in children:
            child_item = widget.item(child_id)
            if child_item['text'] == '<empty>':
                return
            self.tree.delete(child_id)

        self.tree.insert(oid, 'end', text='.',
            tags=repr(item_path))

    def _handle_tortoise(self, item, tortoise_cmd):
        the_path = self._get_selected_path(item)
        le_tortoise.handle_tortoise(the_path, tortoise_cmd)

    def _handle_open_command_prompt(self, item):
        the_path = self._get_selected_path(item)
        the_dir = os.path.dirname(the_path)
        with dde.chdir(the_dir):
            command = my_config.get_open_shell()
            subprocess.Popen(command)

    def _handle_view(self, item):
        the_path = self._get_selected_path(item)
        the_dir = os.path.dirname(the_path)
        if sys.platform == 'win32':
            command = f'explorer {the_dir}'
        elif sys.platform == 'darwin':
            command = f'open {the_path}'
        elif sys.platform == 'linux':
            command = f'xdg-open {the_dir}'
        else:
            dde.le_log(f"Don't know how to open containing folder for {sys.platform}")
        dde.le_log(f'OCF: {command}')
        subprocess.Popen(command, shell=True)


    def _handle_dde(self, item):
        the_path = self._get_selected_path(item)
        if os.path.isdir(the_path):
            the_dir = the_path
        else:
            the_dir = os.path.dirname(the_path)
        if sys.platform == 'win32':
            command = f'dde {the_path}'
        elif sys.platform == 'darwin':
            command = f'open {the_path}'
        elif sys.platform == 'linux':
            command = f'dde'
        else:
            dde.le_log(f"Don't know how to start dde for {sys.platform}")
        dde.le_log(f'OCF: {command}')
        with dde.chdir(the_dir):
            subprocess.Popen(command, shell=True)

    def _get_selected_path(self, item):
        the_item = item['tags']
        the_path = str(the_item[0])
        if len(the_path) == 0:
            return None
        path = the_path[1:-1]  # UNIFY
        dde.le_log(f'_get_selected_path = {path}')
        return path

    def _handle_edit(self, item):
        path = self._get_selected_path(item)
        if path is None:
            return
        command = my_config.get_editor()
        dde.le_log(f'Strt: {command}')
        if 'line' in command:
            command = command.replace('line', '3')
        if 'path' in command:
            command = command.replace('path', path)
        dde.le_log(f'Repl: {command}')
        subprocess.Popen(command, shell=True)

    def set_tabview(self, tabbed_view):
        self.tabbed_view = tabbed_view

    def _handle_find_a_file(self, item):
        path = self._get_selected_path(item)
        faf = FindAFile(self.parent, self.options)
        faf.find_a_file(self.tabbed_view, path)

    def _handle_ripgrep_in_file(self, item):
        path = self._get_selected_path(item)
        faf = FindAFile(self.parent, self.options)
        faf.rg_in_file(self.tabbed_view, path)

    def _handle_tree_view(self, event):
        widget = event.widget
        widget.update()
        iid = widget.focus()
        item = widget.item(iid)
        the_key = event.keysym.lower()
        if the_key == 'b':
            self._handle_tortoise(item, 'blame')
        if the_key == 'l':
            self._handle_tortoise(item, 'log')
        if the_key == 'c':
            self._handle_open_command_prompt(item)
        if the_key == 'e':
            self._handle_edit(item)
        if the_key == 'f':
            self._handle_find_a_file(item)
        if the_key == 'r':
            self._handle_ripgrep_in_file(item)
        if the_key == 'v':
            self._handle_view(item)
        if the_key == 'd':
            self._handle_dde(item)

    def _keyboard_click(self, event):
        dde.le_log("TreeView _keyboard_click")
        self._handle_tree_view(event)

    def _context_menu(self, event):
        ctxMenu = tk.Menu(self.tree, tearoff=0)
        ctxMenu.add_command(label='Blame  (B)',
            command=self._curry_handle_context_menu(event, 'b', ctxMenu))
        ctxMenu.add_command(label='Open Shell Here  (C)',
            command=self._curry_handle_context_menu(event, 'c', ctxMenu))
        ctxMenu.add_command(label='Edit  (E)',
            command=self._curry_handle_context_menu(event, 'e', ctxMenu))
        ctxMenu.add_command(label='Find a File  (F)',
            command=self._curry_handle_context_menu(event, 'f', ctxMenu))
        ctxMenu.add_command(label='Log  (L)',
            command=self._curry_handle_context_menu(event, 'l', ctxMenu))
        ctxMenu.add_command(label='RipGrep in Files  (R)',
            command=self._curry_handle_context_menu(event, 'r', ctxMenu))
        ctxMenu.add_command(label='Open containing folder  (V)',
            command=self._curry_handle_context_menu(event, 'v', ctxMenu))
        ctxMenu.add_command(label='Start dde here  (D)',
            command=self._curry_handle_context_menu(event, 'd', ctxMenu))
        ctxMenu.tk_popup(event.x_root, event.y_root)
        dde.le_log("treeContextMenu")

    def _curry_handle_context_menu(self, event, key, ctxMenu):
        dde.le_log('curry_handleContextEdit')
        widget = event.widget
        widget.update()
        iid = widget.identify_row(event.y)
        item = widget.item(iid)

        def handleContextMenu(the_key=key):
            if the_key == 'b':
                self._handle_tortoise(item, 'blame')
            if the_key == 'c':
                self._handle_open_command_prompt(item)
            if the_key == 'e':
                self._handle_edit(item)
            if the_key == 'f':
                self._handle_find_a_file(item)
            if the_key == 'l':
                self._handle_tortoise(item, 'log')
            if the_key == 'r':
                self._handle_ripgrep_in_file(item)
            if the_key == 'v':
                self._handle_view(item)
            if the_key == 'd':
                self._handle_dde(item)
            # Since we are creating the ctxMenu dynamically we
            # need to destroy it manually
            ctxMenu.destroy()

        return handleContextMenu

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
class FindAFile:
    def __init__(self, parent, options, *args, **kwargs):
        self.parent = parent
        self.options = options

    def _get_file_pattern(self, title=None):
        mb = le_mbox.FindAFileDialog(self.parent, title=title)
        dde.le_log(mb.result)
        return mb.result

    def find_a_file(self, tabbed_view, given_path):
        pattern = self._get_file_pattern('Find a File')
        if pattern is None:   # A Cancel
            return
        faf_list = tabbed_view.faf.get_text_widget()
        faf_list.config(state=tk.NORMAL)
        faf_list.delete('1.0', tk.END)
        # Must assume they are looking for a file in a directory ?
        if os.path.isdir(given_path):
            start_dir = given_path
        else:
            start_dir = os.path.dirname(given_path)
        start = f'Searching for a file matching {pattern} in {start_dir}\n'
        dde.le_log(start)
        faf_list.insert('1.0', start)
        tabbed_view.tabs.select(tabbed_view.faf.get_tab_index())
        hits = 0
        self.parent.config(cursor="clock")
        self.parent.update()

        for root, dirs, files in os.walk(start_dir):
            for file in fnmatch.filter(files, pattern):
                hits = hits + 1
                hit = str(os.path.join(root, file))
                dde.le_log(f'Hit: {hit}')
                faf_list.insert(tk.END, f'{hit}:\n')

        self.parent.config(cursor="")
        if hits == 0:
            end = f'- No hits for \"{pattern}\" -\n'
        else:
            end = f'Hits: {hits}\n'
        faf_list.insert(tk.END, end)
        faf_list.config(state=tk.NORMAL)
        dde.le_log(f'Hits: {hits}')
        tabbed_view.tabs.select(1)

    def rg_in_file(self, tabbed_view, start_dir):
        rgif_list = tabbed_view.rgif.get_text_widget()
        rgif_list.config(state=tk.NORMAL)
        rgif_list.delete('1.0', tk.END)
        if start_dir is None:
            dde.le_log('start_dir was empty')
            start_dir = self.options['actual_dir']
        tabbed_view.tabs.select(tabbed_view.rgif.get_tab_index())
        hits = 0
        find_outputter = curry_fif_output(rgif_list)
        self.parent.config(cursor="clock")
        self.parent.update()
        le_ripgrep.maine(start_dir, self.parent, find_outputter)
        self.parent.config(cursor="")
        rgif_list.config(state=tk.NORMAL)

def curry_fif_output(fif_list):
    fif_list.tag_configure('hi_lite', foreground='red')
    def fif_output(str):
        fif_list.insert(tk.END, f'{str}\n')
    return fif_output

