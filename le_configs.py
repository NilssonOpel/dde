#!/usr/bin/python3
# coding=UTF-8

import json
import os
import shutil
import sys

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def load_json_data(file):
    data = {}
    with open(file) as fp:
        try:
            data = json.load(fp)
        except json.decoder.JSONDecodeError:
            print('loading json went wrong')

    return data

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def store_json_data(file, data):
    try:
        with open(file, 'w') as fp:
            json.dump(data, fp, indent=2)
    except json.decoder.JSONDecodeError:
        print('saving json went wrong')

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def make_a_default_config_file():
    default_config = {}
    default_config['editor'] = 'eps.exe +line "path"'
    default_config['shell'] = 'cmd /c start'

    file_name = get_config_file(True) + ".default"
    store_json_data(file_name, default_config)

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def get_config_file(to_save):
    the_file_name = "dde_config.json"
    script_dir = os.path.dirname(os.path.abspath(__file__))
    the_local_file = the_file_name
    the_global_file = os.path.join(script_dir, the_file_name)
    if to_save:
#        print(f'Select local: {the_local_file}')
        return the_local_file

    if os.path.exists(the_local_file):
#        print(f'Local: {the_local_file} exists - selected')
        return the_local_file
#    print(f'Select global: {the_global_file}')
    return the_global_file

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def get_the_options():
    configs = load_json_data(get_config_file(False))
    return configs

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def save_the_options(options):
    store_json_data(get_config_file(True), options)

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def get_the_config(key, configs=None):
    if not configs:
        configs = load_json_data(get_config_file(False))
    if key in configs:
        return configs[key]

    return None

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def get_editor():
    test = 'eps.exe'
    configged = get_the_config('editor')
    if configged:
        return configged

    if sys.platform == 'win32':
        editor_list = [test, 'emacs.exe', 'gvim.exe', 'notepad.exe']
    else:
        editor_list = [test, 'emacs', 'gvim', 'gedit']

    for editor in editor_list:
        reply = shutil.which(editor)
        if reply is not None:
            return editor

    return None

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def get_open_shell():
    if sys.platform == 'win32':
        return 'cmd /c start'

    return 'gnome-terminal'


#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
if __name__ == '__main__':
    make_a_default_config_file()
