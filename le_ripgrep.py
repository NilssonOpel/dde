import os
import sys
import tkinter as tk

import dde
import le_mbox


#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def grep_files(working_dir, search_pattern,  file_pattern, ignore_case,
               max_depth, color, print_lineno, result_output):
    """Search files and directories."""

    command = ['rg', '--with-filename', '--no-heading']
    if color:
        command.extend(['--color', color])
    if print_lineno:
        command.append('--line-number')
    if ignore_case:
        command.append('--ignore-case')
    if file_pattern:
        command.extend(['--glob', file_pattern])
    if max_depth:
        command.extend(['--max-depth', max_depth])

    command.append(search_pattern)
    if working_dir:
        command.append(working_dir)
    result_output('<' + ' '.join(command) + '>')
    result = dde.run_process(command, False)

    all_lines = result.splitlines()
    for line in all_lines:
#        print(line)
        result_output(line.strip('\n'))

    if not len(all_lines):
        result_output(f'- No hits for \"{search_pattern}\" -')

#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------

def dialog(parent, title_text):
    mb = le_mbox.MyDialog(parent, title=title_text)
    if mb.result is None:  # That is Cancel - emulate a 'normal' response
        return None, None, 0

    return mb.result

#-------------------------------------------------------------------------------
def maine(path, parent, fif_output):
    max_depth = None
    color = 'never'
    print_lineno = True
    abs_path = os.path.abspath(path)

    reply = dialog(parent, 'rg in Files')
    if reply[0] is None:
        return 0
    search_pattern, file_pattern, ignore_case = reply

    grep_files(abs_path, search_pattern, file_pattern, ignore_case,
               max_depth, color, print_lineno, fif_output)

#-------------------------------------------------------------------------------
def main():
    max_depth = None
    color = 'never'  # 'ansi' how to print 'ansi' output?
    print_lineno = True

    root = tk.Tk()
    root.withdraw() # Hide root window

    reply = dialog(root, 'rg parameters')
    if reply[0] is None:
        return 0

    search_pattern, file_pattern, ignore_case = reply

    output_function = print
    grep_files(None, search_pattern, file_pattern, ignore_case,
               max_depth, color, print_lineno, output_function)

#-------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
